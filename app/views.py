from django.shortcuts import render
from django.http import HttpResponse
from django.views import View
import os
from django.views.generic import TemplateView
from django.utils.translation import gettext as _

# class MultiLanguegesSupportMixin(View):
#     def dispatch(self, request, *args, **kwargs):
#         self.template_name = os.path.join(request.template_dir,self.template_name)
#         return super().dispatch(request, *args, **kwargs)


# class HomeView(TemplateView):
#     template_name = "home.html"

def HomeView(request):
    # from django.utils import translation
    # user_language = 'fi'
    # translation.activate(user_language)
    # request.session[translation.LANGUAGE_SESSION_KEY] = user_language
    # if translation.LANGUAGE_SESSION_KEY in request.session:
    #     del request.session[translation.LANGUAGE_SESSION_KEY]

    title = _('HOMEPAGE')
    return render(request,'home.html',{'title':title})

class AboutView(TemplateView):
    template_name = "about.html"

class TestView(TemplateView):
    template_name = "test.html"


