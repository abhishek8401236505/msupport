from django.contrib import admin
from django.urls import path
from app import views
from django.conf.urls import url

urlpatterns = [
   path('',views.HomeView,name="home"),
   path('about/',views.AboutView.as_view(),name="about"),
   path('test/',views.TestView.as_view(),name="test"),
]
